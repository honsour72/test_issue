FROM python:latest
COPY . .
RUN pip install poetry
RUN chmod +x ./docker-entrypoint.sh
CMD ["./docker-entrypoint.sh"]
EXPOSE 8000