from datetime import datetime
from pydantic import BaseModel


class Message(BaseModel):
    id: int
    create_at: datetime
    status: str
    distribution_id: int
    client_id: int

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "create_at": datetime.strptime('20.05.2023-00:00:00', "%d.%m.%Y-%H:%M:%S"),
                "status": "sent",
                "distribution_id": 1,
                "client_id": 2
            }
        }


class Client(BaseModel):
    id: int = None
    phone: int
    code: int
    tag: str
    timezone: int

    class Config:
        orm_mode = True


class Distribution(BaseModel):
    id: int | None = None
    start_at: datetime
    text: str
    filter: str | None = None
    deadline: datetime
    interval: str
    done: bool | None = False

    class Config:
        orm_mode = True


class StatisticsResponse(BaseModel):
    id: int
    total_messages: int
    message_stats: dict

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "total_messages": 20,
                "message_stats": {
                    "sent": 15,
                    "delay": 3,
                    "reject": 2
                }
            }
        }
