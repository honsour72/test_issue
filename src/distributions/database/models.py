from datetime import datetime
from sqlalchemy import String, DateTime, Column, Enum, Text, ForeignKey, Boolean, BigInteger, Integer
from sqlalchemy.orm import DeclarativeBase, relationship


MESSAGE_SENT = 'sent'
MESSAGE_REJECTED = 'rejected'
MESSAGE_OVERTIME = 'overtime'


class Base(DeclarativeBase):
    pass


class Distribution(Base):
    __tablename__ = "distributions"

    id = Column(BigInteger, primary_key=True)
    start_at = Column(DateTime(timezone=False), default=datetime.now, nullable=False)
    text = Column(Text, nullable=True)
    filter = Column(Text, nullable=True)
    deadline = Column(DateTime(timezone=False), nullable=False)
    interval = Column(String(11), nullable=False)
    done = Column(Boolean, nullable=False, default=False)
    messages = relationship("Message", back_populates="distribution", cascade="all, delete, delete-orphan")

    def __repr__(self):
        return f"<Distribution(id={self.id}, " \
               f"start={self.start_at}, end={self.deadline}, filter={self.filter}, interval={self.interval})>"


class Client(Base):
    __tablename__ = "clients"

    id = Column(BigInteger, primary_key=True)
    phone = Column(BigInteger, nullable=False)
    code = Column(Integer)
    tag = Column(Text, nullable=True)
    timezone = Column(Integer, nullable=True, default=3)
    messages = relationship("Message", back_populates="client", cascade="all, delete, delete-orphan")

    def __repr__(self):
        return f"<Client(id={self.id}, {self.phone} ({self.code}), tag={self.tag}, " \
               f"UTC{'+' if self.timezone > 0 else '-'}{abs(self.timezone)})>"


class Message(Base):
    __tablename__ = "messages"

    id = Column(BigInteger, primary_key=True)
    create_at = Column(DateTime(timezone=False), default=datetime.now, nullable=False)
    status = Column(Enum(MESSAGE_SENT, MESSAGE_REJECTED, MESSAGE_OVERTIME, name='message_status'),
                    server_default=MESSAGE_SENT,
                    nullable=False)
    distribution_id = Column(Integer, ForeignKey("distributions.id"), nullable=True)
    client_id = Column(Integer, ForeignKey("clients.id"), nullable=True)
    client = relationship("Client", back_populates="messages")
    distribution = relationship('Distribution', back_populates='messages')
    
    def __repr__(self):
        return f"<Message(id={self.id}, from distribution: {self.distribution_id}, " \
               f"status={self.status}, for client with id: {self.client_id} created: {self.create_at})>"
