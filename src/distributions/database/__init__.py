from distributions import db_uri
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session


engine = create_engine(db_uri)
DBSession = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_session() -> Session:
    session = DBSession()
    try:
        yield session
    finally:
        session.close()
