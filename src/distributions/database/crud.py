from . import models, schemas
from distributions import distribution_log, client_log
from distributions.distributor import Distributor
from fastapi import BackgroundTasks, HTTPException
from sqlalchemy import func
from sqlalchemy.orm import Session
from typing import Type


# ================================================ CLIENT SECTION ==================================================== #


def get_client_item(session: Session, client_id: int) -> models.Client:
    client_item = session.query(models.Client).get(client_id)
    if client_item:
        return client_item
    else:
        raise HTTPException(status_code=404, detail=f"Client with id={client_id} was not found")


def update_client_item_attribute(session: Session, client_id: int, attribute: str, value: str | int) -> dict:
    client = session.query(models.Client).get(client_id)
    if client:
        if hasattr(client, attribute):
            setattr(client, attribute, value)
            session.commit()
            status = f"Attribute {attribute} of client with id={client_id} successfully changed on: {value}"
            client_log.info(status)
            return {"status": status}

        err_422 = f"Client with id={client_id} has no attribute {attribute}"
        client_log.exception(err_422)
        raise HTTPException(status_code=422, detail=err_422)

    err_404 = f"Client with id={client_id} was not found"
    client_log.exception(err_404)
    raise HTTPException(status_code=404, detail=err_404)


def create_client_item(session: Session, client: schemas.Client) -> schemas.Client:
    client_item = models.Client(**client.dict())
    session.add(client_item)
    session.commit()
    session.refresh(client_item)
    client_log.info(f"Add new client: {client_item}")
    return client_item


def delete_client_item(session: Session, client_id: int) -> dict:
    client = session.query(models.Client).get(client_id)
    if client:
        session.delete(client)
        session.commit()
        status = f"Client with id={client_id} successfully removed"
        client_log.info(status)
        return {"status": status}

    err_404 = f"Client with id={client_id} was not found"
    client_log.exception(err_404)
    raise HTTPException(status_code=404, detail=err_404)


# ========================================== DISTRIBUTIONS SECTION =================================================== #


def get_distribution_item(session: Session, distribution_id: int) -> schemas.Distribution:
    distribution_item = session.query(models.Distribution).get(distribution_id)
    if distribution_item:
        return distribution_item
    raise HTTPException(status_code=404, detail=f"Distribution with id={distribution_id} was not found")


def get_distribution_item_statistics(session: Session, distribution_id: int) -> list[Type[models.Message]]:
    return session.query(models.Message).filter_by(distribution_id=distribution_id).all()


def update_distribution_item_attribute(session: Session,
                                       distribution_id: int,
                                       attribute: str,
                                       value: str | int) -> dict:
    distribution = session.query(models.Distribution).get(distribution_id)
    if distribution:
        if hasattr(distribution, attribute):
            setattr(distribution, attribute, value)
            session.commit()
            status = f"Attribute {attribute} of distribution with id={distribution_id} successfully changed on: {value}"
            distribution_log.info(status)
            return {"status": status}

        err_422 = f"Distributions with id={distribution_id} has no attribute {attribute}"
        distribution_log.exception(err_422)
        raise HTTPException(status_code=422, detail=err_422)

    err_404 = f"Distribution with id={distribution_id} was not found"
    distribution_log.exception(err_404)
    raise HTTPException(status_code=404, detail=err_404)


def create_distribution_item(background_tasks: BackgroundTasks,
                             session: Session,
                             distribution: schemas.Distribution) -> schemas.Distribution:
    distribution_item = models.Distribution(**distribution.dict())
    session.add(distribution_item)
    session.commit()
    session.refresh(distribution_item)
    distribution_log.info(f"New Distribution added: {distribution_item}")
    background_tasks.add_task(Distributor.distribute, session, distribution_item.id)
    return distribution_item


def delete_distribution_item(session: Session, distribution_id: int) -> dict:
    distribution = session.query(models.Distribution).get(distribution_id)
    if distribution:
        session.delete(distribution)
        session.commit()
        status = f"Distribution with id={distribution_id} successfully removed"
        distribution_log.info(status)
        return {"status": status}

    err_404 = f"Distribution with id={distribution_id} was not found"
    distribution_log.exception(err_404)
    raise HTTPException(status_code=404, detail=err_404)


# =============================================== OTHER SECTION ====================================================== #


def get_distributions_statistics(session: Session) -> list[dict]:
    result = []
    q = session.query(models.Distribution.id, models.Message.status, func.count(models.Message.id))\
        .join(models.Message).group_by(models.Distribution, models.Message.status).all()
    unique_ids = list(set([r[0] for r in q]))
    for unique_id in unique_ids:
        raw_message_stats = [d[1:] for d in q if d[0] == unique_id]
        total_messages = sum([x[-1] for x in raw_message_stats])
        message_stats = dict(raw_message_stats)
        distribution_data = {
            "id": unique_id,
            "total_messages": total_messages,
            "message_stats": message_stats
        }
        result.append(distribution_data)
    return result
