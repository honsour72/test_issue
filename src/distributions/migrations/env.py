from sqlalchemy import engine_from_config
from sqlalchemy import pool
from distributions.database.models import Base
from distributions import script_location, prepend_sys_path, version_path_separator, db_uri
from alembic import context


config = context.config
config.set_main_option('sqlalchemy.url', db_uri)
config.set_main_option('script_location', script_location)
config.set_main_option('prepend_sys_path', prepend_sys_path)
config.set_main_option('version_path_separator', version_path_separator)
target_metadata = Base.metadata


def run_migrations_offline() -> None:
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    connectable = engine_from_config(
        config.get_section(config.config_ini_section, {}),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
