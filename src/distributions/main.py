from .distributor import run_distributor_process
from distributions.database import get_session
from distributions.database.crud import *
from distributions.database.schemas import *
from distributions.database.models import Client as ClientModel
from distributions.database.models import Distribution as DistributionModel
from fastapi import Depends, FastAPI, BackgroundTasks
from sqlalchemy.orm import Session
from typing import Type
import asyncio


app = FastAPI(
    title="API Distributions service",
    openapi_tags=[
        {"name": "Clients", "description": "Information about all clients"},
        {"name": "Distributions", "description": "Information about every distribution + statistics"},
        {"name": "Statistics", "description": "Provide statistics information about whole service"}
    ]
)


@app.on_event('startup')
async def run_distributor():
    # Run service which monitors distributions queue and serve them in time
    asyncio.create_task(run_distributor_process())


# ================================================ CLIENT SECTION ==================================================== #


@app.get(
    "/v1/clients",
    name="Get all Clients",
    description="Return all clients in database",
    tags=['Clients'],
    response_model=list[Client]
)
async def get_clients(session: Session = Depends(get_session)) -> list[Type[ClientModel]]:
    return session.query(ClientModel).all()


@app.get(
    "/v1/clients/{client_id}",
    name="Get Client by id",
    description="Return concrete client",
    tags=['Clients'],
    responses={
        404: {
            "description": "Client was not found",
            "content": {
                "application/json": {
                    "example": {"status": "Client with id=1 was not found"}
                }
            }
        },
        200: {
            "model": Client,
            "description": "Client requested by id",
            "content": {
                "application/json": {
                    "example": {"id": 1, "phone": 71234567890, "code": 123, "tag": "gaming", "timezone": 3}
                }
            },
        },
    },
)
async def get_client(client_id: int, session: Session = Depends(get_session)) -> Client:
    return get_client_item(session=session, client_id=client_id)


@app.put(
    "/v1/clients/{client_id}",
    tags=["Clients"],
    responses={
        422: {
            "description": "Client has no attribute",
            "content": {
                "application/json": {
                    "example": {"status": "Client with id=1 has no attribute <attribute_name>"}
                }
            }
        },
        404: {
            "description": "Client was not found",
            "content": {
                "application/json": {
                    "example": {"status": "Client with id=1 was not found"}
                }
            }
        },
        200: {
            "model": Client,
            "description": "Client edited",
            "content": {
                "application/json": {
                    "example": {"status": "Client's (id=1) attribute <attribute_name> changed on: <value>"}
                }
            },
        },
    },
)
async def update_client_attribute(client_id: int, attribute: str, value: str | int,
                                  session: Session = Depends(get_session)) -> dict:
    return update_client_item_attribute(session=session, client_id=client_id, attribute=attribute, value=value)


@app.post(
    '/v1/clients/create',
    tags=["Clients"],
    responses={
        422: {
            "description": "Client fields validation error",
            "content": {
                "application/json": {
                    "example": {"status": "Request body have errors"}
                }
            }
        },
        200: {
            "model": Client,
            "description": "Client created successfully",
            "content": {
                "application/json": {
                    "example": {"id": 1, "phone": 71234567890, "code": 123, "tag": "gaming", "timezone": 3}
                }
            },
        },
    },
)
async def create_client(client: Client, session: Session = Depends(get_session)) -> Client:
    return create_client_item(session=session, client=client)


@app.delete(
    '/v1/clients/{client_id}/delete',
    tags=["Clients"],
    responses={
        404: {
            "description": "Client to delete was not found",
            "content": {
                "application/json": {
                    "example": {"status": "There is no client with id=<ID>"}
                }
            }
        },
        200: {
            "model": Client,
            "description": "Client deleted successfully",
            "content": {
                "application/json": {
                    "example": {"status": "Client with id=<ID> successfully removed"}
                }
            },
        },
    },
)
async def delete_client(client_id: int, session: Session = Depends(get_session)) -> dict:
    return delete_client_item(session=session, client_id=client_id)


# ========================================== DISTRIBUTIONS SECTION =================================================== #


@app.get("/v1/distributions", tags=['Distributions'], response_model=list[Distribution])
async def get_distributions(session: Session = Depends(get_session)) -> list[Type[Distribution]]:
    return session.query(DistributionModel).all()


@app.get(
    "/v1/distributions/{distribution_id}",
    tags=['Distributions'],
    responses={
        404: {
            "description": "Distribution was not found",
            "content": {
                "application/json": {
                    "example": {"status": "Distribution with id=1 was not found"}
                }
            }
        },
        200: {
            "model": Distribution,
            "description": "Distribution requested by id",
            "content": {
                "application/json": {
                    "example": {
                        "id": 1,
                        "start_at": datetime.strptime('20.05.2023-00:00:00', "%d.%m.%Y-%H:%M:%S"),
                        "text": "Hello world",
                        "filters": None,
                        "deadline": datetime.strptime('25.05.2023-00:00:00', "%d.%m.%Y-%H:%M:%S"),
                        "interval": "09:00-19:00"
                    }
                }
            },
        },
    },
)
async def get_distribution(distribution_id: int, session: Session = Depends(get_session)) -> Distribution:
    return get_distribution_item(session=session, distribution_id=distribution_id)


@app.get(
    "/v1/distributions/{distribution_id}/statistics",
    tags=['Distributions'],
    response_model=list[Message],
    responses={
        404: {
            "description": "Distribution was not found",
            "content": {
                "application/json": {
                    "example": {"status": "Distribution with id=<ID> was not found"}
                }
            }
        },
    }
)
async def get_distribution_statistics(
        distribution_id: int,
        session: Session = Depends(get_session)
) -> list[Type[Message]]:
    return get_distribution_item_statistics(session=session, distribution_id=distribution_id)


@app.put(
    "/v1/distributions/{distribution_id}",
    tags=["Distributions"],
    responses={
        422: {
            "description": "Distribution has no attribute",
            "content": {
                "application/json": {
                    "example": {"status": "Distributions with id=1 has no attribute <attribute_name>"}
                }
            }
        },
        404: {
            "description": "Distribution was not found",
            "content": {
                "application/json": {
                    "example": {"status": "Distribution with id=1 was not found"}
                }
            }
        },
        200: {
            "model": Distribution,
            "description": "Distribution edited successfully",
            "content": {
                "application/json": {
                    "example": {"status": "Distribution's (id=1) attribute <attribute_name> changed on: <value>"}
                }
            },
        },
    },
)
async def update_distribution_attribute(distribution_id: int, attribute: str, value: str | int,
                                        session: Session = Depends(get_session)) -> dict:
    return update_distribution_item_attribute(session, distribution_id, attribute=attribute, value=value)


@app.post(
    "/v1/distributions/create",
    tags=['Distributions'],
    responses={
        422: {
            "description": "Distribution fields validation error",
            "content": {
                "application/json": {
                    "example": {"status": "Request body have errors"}
                }
            }
        },
        200: {
            "model": Distribution,
            "description": "Distribution created successfully",
            "content": {
                "application/json": {
                    "example": {
                        "id": 1,
                        "start_at": '20.05.2023 10:00:00',
                        "text": "Hello world",
                        "filters": None,
                        "deadline": '25.05.2023 20:00:00',
                        "interval": "09:00-19:00"
                    }
                }
            },
        },
    },
)
async def create_distribution(background_tasks: BackgroundTasks,
                              distribution: Distribution,
                              session: Session = Depends(get_session),
                              ) -> Distribution:
    return create_distribution_item(session=session, distribution=distribution, background_tasks=background_tasks)


@app.delete(
    '/v1/distributions/{distribution_id}/delete',
    tags=["Distributions"],
    responses={
        404: {
            "description": "Distribution to delete was not found",
            "content": {
                "application/json": {
                    "example": {"status": "There is no Distribution with id=<ID>"}
                }
            }
        },
        200: {
            "model": Distribution,
            "description": "Distribution deleted successfully",
            "content": {
                "application/json": {
                    "example": {"status": "Distribution with id=<ID> successfully removed"}
                }
            },
        },
    },
)
async def delete_distribution(distribution_id: int, session: Session = Depends(get_session)) -> dict:
    return delete_distribution_item(session=session, distribution_id=distribution_id)


# =============================================== OTHER SECTION ====================================================== #

@app.get(
    '/v1/statistics',
    tags=["Statistics"],
    response_model=list[StatisticsResponse]
)
async def get_statistics(session: Session = Depends(get_session)) -> list:
    return get_distributions_statistics(session)
