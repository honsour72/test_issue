import os
from dotenv import load_dotenv
from loguru import logger as log


load_dotenv()
LOG_FORMAT = "{time:YYYY:MMM:DD:ddd:HH:mm:ss} | {level} | {name}:{function}:{line} | {message}"
log.add(
    "log/distributions.log",
    colorize=True,
    format=LOG_FORMAT,
    enqueue=True,
    filter=lambda record: record["extra"]["task"] == "distribution"
)
log.add(
    "log/clients.log",
    colorize=True,
    format=LOG_FORMAT,
    enqueue=True,
    filter=lambda record: record["extra"]["task"] == "client"
)
log.add(
    "log/messages.log",
    colorize=True,
    format=LOG_FORMAT,
    enqueue=True,
    filter=lambda record: record["extra"]["task"] == "message"
)
distribution_log = log.bind(task='distribution')
client_log = log.bind(task='client')
message_log = log.bind(task='message')

postgres_password = os.environ.get("postgres_password".upper())
postgres_user = os.environ.get("postgres_user".upper())
postgres_db = os.environ.get("postgres_db".upper())
db_host = os.environ.get("db_host".upper())
db_port = os.environ.get("db_port".upper())
db_uri = f"postgresql://{postgres_user}:{postgres_password}@{db_host}:{db_port}/{postgres_db}"
script_location = os.environ.get("script_location".upper())
prepend_sys_path = os.environ.get("prepend_sys_path".upper())
version_path_separator = os.environ.get("version_path_separator".upper())
middleware_message_uri = os.environ.get("middleware_message_uri".upper())
jwt_token = os.environ.get("jwt_token".upper())
