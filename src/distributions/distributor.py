from datetime import datetime
from distributions import distribution_log, message_log, jwt_token, middleware_message_uri
from distributions.database import DBSession
from distributions.database.models import Distribution, Client, Message, MESSAGE_SENT, MESSAGE_REJECTED
from sqlalchemy.orm import Session
from typing import Type
import asyncio
import requests


class Distributor:
    """
    The base class for sending distributions
    """
    @staticmethod
    def check_active_distributions(session: Session) -> list[Type[Distribution]]:
        return session.query(Distribution).filter_by(done=False).order_by(Distribution.start_at).all()

    async def run(self):
        session = DBSession()
        while True:
            not_distributed = self.check_active_distributions(session)
            for distribution in not_distributed:
                self.distribute(session, distribution.id)
            await asyncio.sleep(1)

    @staticmethod
    def distribute(session: Session, distribution_id: int) -> None:
        distribution = session.query(Distribution).get(distribution_id)
        tag, text = distribution.filter, distribution.text
        start_at, deadline = distribution.start_at, distribution.deadline
        current_time = datetime.now()
        if start_at < current_time < deadline:
            clients = session.query(Client).filter_by(tag=tag).all()
            if clients:
                distribution_log.debug(
                    f"Start distribution (id={distribution_id}) for {len(clients)} client(s). "
                    f"Text: {text}, filter: {tag}"
                )
                for client in clients:
                    status = MESSAGE_REJECTED  # default bruh
                    response_status = queue_message_sending(client.id, client.phone, text)
                    if response_status:
                        status = MESSAGE_SENT
                    new_message: Message = Message(
                        create_at=datetime.now(),
                        status=status,
                        distribution_id=distribution_id,
                        client_id=client.id
                    )
                    message_log.info(f"Add new message: {new_message}")
                    session.add(new_message)
            else:
                distribution_log.info(f"There is no clients for distribution with id={distribution_id}")
            distribution.done = True
            session.commit()


def queue_message_sending(client_id: int, client_phone: int, text: str):
    base_url = middleware_message_uri + str(client_id)
    headers = {"Authorization": f"Bearer {jwt_token}", "Content-Type": "application/json", "accept": "application/json"}
    data = {"id": client_id, "phone": client_phone, "message": text}
    message_log.debug(f"Sending request... (Client id: {client_id})")
    r = requests.post(base_url, data=data, headers=headers)
    message_log.debug(f"Fetching response: It says: {r.text}")
    return True if r.status_code == 200 else False


async def run_distributor_process():
    await Distributor().run()

