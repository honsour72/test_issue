#!/bin/bash

echo "Configure environment"
poetry config virtualenvs.path --unset
poetry config virtualenvs.in-project true
poetry install

# Apply database migrations
sleep 7
echo "Apply database migrations"
poetry run alembic -c ./src/distributions/alembic.ini upgrade head

# Start server
echo "Starting server"
poetry run uvicorn src.distributions.main:app --host 0.0.0.0 --port 8000