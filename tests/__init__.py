from fastapi.testclient import TestClient
from distributions.main import app


BASE_URL = "http://localhost:8000/"
client = TestClient(app)
