import json
from . import BASE_URL, client


class TestDistributionSuccessCase:
    distribution_id = 1
    attribute = "filter"
    value = "movie"
    text = "Hello world"
    _filter = "gaming"
    headers = {"Content-type": "Application/json"}

    def test_success_create(self):
        endpoint = "v1/distributions/create"
        data = {
            "id": self.distribution_id,
            "start_at": "2023-05-20T00:00:00",
            "text": self.text,
            "filter": self._filter,
            "deadline": "2023-05-25T00:00:00",
            "interval": "09:00-19:00",
        }
        url = BASE_URL + endpoint
        response = client.post(url, content=json.dumps(data))
        # response = requests.post(url, data=data, headers={"Content-Type": "Application/json"})
        assert response.status_code == 200
        assert response.json().get('id') == self.distribution_id
        assert response.json().get('text') == self.text

    def test_success_get(self):
        endpoint = f"v1/distributions/{self.distribution_id}?"
        url = BASE_URL + endpoint
        response = client.get(url)
        assert response.status_code == 200
        assert response.json().get('id') == self.distribution_id

    def test_success_update(self):
        endpoint = f"v1/distributions/{self.distribution_id}?"
        data = {
            "attribute": self.attribute,
            "value": self.value
        }
        url = BASE_URL + endpoint + "&".join([f"{k}={v}" for k, v in data.items()])
        response = client.put(url, headers=self.headers)
        assert response.status_code == 200
        assert self.value in response.json().get('status')

    def test_success_delete(self):
        endpoint = f"v1/distributions/{self.distribution_id}/delete"
        url = BASE_URL + endpoint
        response = client.delete(url)
        assert response.status_code == 200
        assert str(self.distribution_id) in response.json().get("status")
