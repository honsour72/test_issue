import json
from . import BASE_URL, client


class TestClientSuccessCase:
    client_id = 1
    attribute = 'tag'
    value = 'lammer'
    headers = {"Content-type": "Application/json"}

    def test_success_create(self):
        endpoint = "v1/clients/create"
        data = {
            "id": self.client_id,
            "phone": 71234567890,
            "code": 911,
            "tag": "nocoder",
            "timezone": 3,
        }
        url = BASE_URL + endpoint
        response = client.post(url, content=json.dumps(data))
        assert response.status_code == 200
        assert response.json().get('id') == self.client_id

    def test_create_with_wrong_code(self):
        endpoint = "v1/clients/create"
        data = {
            "id": self.client_id + 1,
            "phone": 71234567890,
            "code": '',
            "tag": "nocoder",
            "timezone": 3,
        }
        url = BASE_URL + endpoint
        response = client.post(url, content=json.dumps(data))
        assert response.status_code == 422
        assert "detail" in response.json()

    def test_create_with_wrong_phone(self):
        endpoint = "v1/clients/create"
        data = {
            "id": self.client_id + 1,
            "phone": '',
            "code": 123,
            "tag": "nocoder",
            "timezone": 3,
        }
        url = BASE_URL + endpoint
        response = client.post(url, content=json.dumps(data))
        assert response.status_code == 422
        assert "detail" in response.json()

    def test_create_with_wrong_timezone(self):
        endpoint = "v1/clients/create"
        data = {
            "id": self.client_id + 1,
            "phone": 71234567890,
            "code": 123,
            "tag": "nocoder",
            "timezone": 'KEK',
        }
        url = BASE_URL + endpoint
        response = client.post(url, content=json.dumps(data))
        assert response.status_code == 422
        assert "detail" in response.json()

    def test_create_without_enough_data(self):
        endpoint = "v1/clients/create"
        data = {
            "id": self.client_id + 100,
            'code': 123,
            "tag": "lox",
            "timezone": 1
        }
        url = BASE_URL + endpoint
        response = client.post(url, content=json.dumps(data))
        assert response.status_code == 422
        assert "detail" in response.json()

    def test_success_get(self):
        endpoint = f"v1/clients/{self.client_id}?"
        url = BASE_URL + endpoint
        response = client.get(url)
        assert response.status_code == 200
        assert response.json().get('id') == self.client_id

    def test_failed_get(self):
        endpoint = f"v1/clients/{self.client_id + 2000}?"
        url = BASE_URL + endpoint
        response = client.get(url)
        assert response.status_code == 404
        assert "not found" in response.json().get('detail')

    def test_success_update(self):
        endpoint = f"v1/clients/{self.client_id}?"
        data = {
            "attribute": self.attribute,
            "value": self.value
        }
        url = BASE_URL + endpoint + "&".join([f"{k}={v}" for k, v in data.items()])
        response = client.put(url, headers=self.headers)
        assert response.status_code == 200
        assert self.value in response.json().get('status')

    def test_unknown_attribute_update(self):
        endpoint = f"v1/clients/{self.client_id}?"
        data = {
            "attribute": "xz",
            "value": self.value
        }
        url = BASE_URL + endpoint + "&".join([f"{k}={v}" for k, v in data.items()])
        response = client.put(url, headers=self.headers)
        assert response.status_code == 422
        assert "xz" in response.json().get('detail')

    def test_update_attribute_unknown_id(self):
        endpoint = f"v1/clients/{self.client_id + 111}?"
        data = {
            "attribute": self.attribute,
            "value": self.value
        }
        url = BASE_URL + endpoint + "&".join([f"{k}={v}" for k, v in data.items()])
        response = client.put(url, headers=self.headers)
        assert response.status_code == 404
        assert "not found" in response.json().get('detail')

    def test_success_delete(self):
        endpoint = f"v1/clients/{self.client_id}/delete"
        url = BASE_URL + endpoint
        response = client.delete(url)
        assert response.status_code == 200
        assert str(self.client_id) in response.json().get("status")

    def test_failed_delete(self):
        endpoint = f"v1/clients/{self.client_id + 404}/delete"
        url = BASE_URL + endpoint
        response = client.delete(url)
        assert response.status_code == 404
        assert "not found" in response.json().get('detail')
