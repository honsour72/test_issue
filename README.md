## Тестовое задание:

### Deploy
1) Создать файл `.env` в корневой директории проекта следующего содержимого
```commandline
POSTGRES_PASSWORD=your_password
POSTGRES_USER=user
POSTGRES_DB=distributor
DB_HOST=db
DB_PORT=5432
MIDDLEWARE_MESSAGE_URI=https://probe.fbrq.cloud/v1/send/
JWT_TOKEN=your_token
PREPEND_SYS_PATH=.
SCRIPT_LOCATION=src/distributions/migrations
VERSION_PATH_SEPARATOR=os
```
2)
```commandline
docker-compose up --build
```
3) Остановка + подчистка контейнеров:
```commandline
docker-compose down
```

### Запуск тестов

1)
```
pip install poetry
```
2)
```
poetry install
```
3)
```commandline
poetry run pytest
```

### Архитектура проекта

```
.
├── docker-compose.yaml
├── docker-entrypoint.sh
├── Dockerfile
├── poetry.lock
├── pyproject.toml
├── README.md
├── src
│   └── distributions
│       ├── alembic.ini
│       ├── database         <--- всё что связано с бд
│       │   ├── crud.py      <--- операции с сущностями
│       │   ├── __init__.py
│       │   ├── models.py    <--- сами модели
│       │   └── schemas.py   <--- схемы для swagger'а
│       ├── distributor.py   <--- сервис рассылки, с отложенным запуском
│       ├── __init__.py
│       ├── main.py          <--- само fastapi приложение
│       └── migrations       <--- миграции бд на основе alembic
│           ├── env.py
│           ├── __pycache__
│           ├── README
│           ├── script.py.mako
│           └── versions
└── tests
    ├── __init__.py
    ├── test_client.py
    └── test_distribution.py

```

### Реализованные ендпоинты API

| №   | Название                                     | Метод  | Ендпоинт                                         |
|-----|----------------------------------------------|--------|--------------------------------------------------|
| 1   | Получение всех клиентов                      | GET    | `/v1/clients`                                    |
| 2   | Получение клиента по id                      | GET    | `/v1/clients/{client_id}`                        |
| 3   | Обновление атрибута клиента по id            | PUT    | `/v1/clients/{client_id}`                        |
| 4   | Создание клиента                             | POST   | `/v1/clients/create`                             |
| 5   | Удаление клиента по id                       | DELETE | `/v1/clients/{client_id}/delete`                 |
| 6   | Получение всех рассылок                      | GET    | `/v1/distributions`                              |
| 7   | Получение рассылки по id                     | GET    | `/v1/distributions/{distribution_id}`            |
| 8   | Получение статистики рассылки по id          | GET    | `/v1/distributions/{distribution_id}/statistics` |
| 9   | Обновление атрибута рассылки по id           | PUT    | `/v1/distributions/{distribution_id}`            |
| 10  | Создание рассылки                            | POST   | `/v1/distributions/create`                       |
| 11  | Удаление рассылки по id                      | DELETE | `/v1/distributions/{distribution_id}/delete`     |
| 12  | Получение общей статистики по всем рассылкам | DELETE | `/v1/statistics`                                 |

### Используемые библиотеки
| Номер | Название           | Версия   | описание                                                                                                |
|-------|--------------------|----------|---------------------------------------------------------------------------------------------------------|
| 1     | aiohttp            | 3.8.4    | Async http client/server framework (asyncio)                                                            |
| 2     | aiopg              | 1.4.0    | Postgres integration with asyncio.                                                                      |
| 3     | aiosignal          | 1.3.1    | aiosignal: a list of registered asynchronous callbacks                                                  |
| 4     | alembic            | 1.11.1   | A database migration tool for SQLAlchemy.                                                               |
| 5     | anyio              | 3.6.2    | High level compatibility layer for multiple asynchronous event loop implementations                     | 
| 6     | async-timeout      | 4.0.2    | Timeout context manager for asyncio programs                                                            |
| 7     | attrs              | 23.1.0   | Classes Without Boilerplate                                                                             |
| 8     | certifi            | 2023.5.7 | Python package for providing Mozilla's CA Bundle.                                                       |
| 9     | charset-normalizer | 3.1.0    | The Real First Universal Charset Detector. Open, modern and actively maintained alternative to Chardet. |
| 10    | click              | 8.1.3    | Composable command line interface toolkit                                                               |
| 11    | fastapi            | 0.95.2   | FastAPI framework, high performance, easy to learn, fast to code, ready for production                  |
| 12    | frozenlist         | 1.3.3    | A list-like structure which implements collections.abc.MutableSequence                                  |
| 13    | h11                | 0.14.0   | A pure-Python, bring-your-own-I/O implementation of HTTP/1.1                                            |
| 14    | httpcore           | 0.17.1   | A minimal low-level HTTP client.                                                                        |
| 15    | httpx              | 0.24.1   | The next generation HTTP client.                                                                        |
| 16    | idna               | 3.4      | Internationalized Domain Names in Applications (IDNA)                                                   |
| 17    | iniconfig          | 2.0.0    | brain-dead simple config-ini parsing                                                                    |
| 18    | loguru             | 0.7.0    | Python logging made (stupidly) simple                                                                   |
| 19    | mako               | 1.2.4    | A super-fast templating language that borrows the best ideas from the existing templating languages.    |
| 20    | markupsafe         | 2.1.2    | Safely add untrusted strings to HTML/XML markup.                                                        |
| 21    | multidict          | 6.0.4    | multidict implementation                                                                                |
| 22    | packaging          | 23.1     | Core utilities for Python packages                                                                      |
| 23    | pluggy             | 1.0.0    | plugin and hook calling mechanisms for python                                                           |
| 24    | psycopg2-binary    | 2.9.6    | psycopg2 - Python-PostgreSQL Database Adapter                                                           |
| 25    | pydantic           | 1.10.7   | Data validation and settings management using python type hints                                         |
| 26    | pytest             | 7.3.1    | pytest: simple powerful testing with Python                                                             |
| 27    | pytest-asyncio     | 0.21.0   | Pytest support for asyncio                                                                              |
| 28    | requests           | 2.31.0   | Python HTTP for Humans.                                                                                 |
| 29    | sniffio            | 1.3.0    | Sniff out which async library your code is running under                                                |
| 30    | sqlalchemy         | 2.0.15   | Database Abstraction Library                                                                            |
| 31    | starlette          | 0.27.0   | The little ASGI library that shines.                                                                    |
| 32    | typing-extensions  | 4.5.0    | Backported and Experimental Type Hints for Python 3.7+                                                  |
| 33    | urllib3            | 2.0.2    | HTTP library with thread-safe connection pooling, file post, and more.                                  |
| 34    | uvicorn            | 0.22.0   | The lightning-fast ASGI server.                                                                         |
| 35    | yarl               | 1.9.2    | Yet another URL library                                                                                 |


